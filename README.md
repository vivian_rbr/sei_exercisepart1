# TRASE DATA SCIENTIST EXERCISE
# Part 1: Programming skills

The data for the first exercise were stored in gitlab (https://gitlab.com/vivian_rbr/sei_exercisepart1) in Part1_VivianRibeiro in order to make it possible to access all the data, including the project folder, which can be read in visual studio and other IDEs, as well as generated scripts, tables and databases storing the csv files. The database was in order to show how to share the databases generated with other colleagues. I am using SQLite as the database for this project. SQLite is ideal for small applications and can be easily manipulated using the DB Browser program, however it may not be suitable for large applications with databases that exceed 100 gigabytes. It can be associated with applications in Python3 using a module called sqlite3 with a very powerful integration with pandas.

## Getting Started
files description:

* Part1_VivianRibeiro.py	** Python script with the routines to scrap the data **
* Part1_VivianRibeiro.pyproj	** py project **
* sqlite_datasetstore.py ** Python script with the routines to create and store the datasets inside the sqlite database **
* SEIExercise_Part1.sqlite	** SQLite with the 3 datasets required **
* VIVIANRIBEIRO_Part1_Dataset1.csv	** Dataset 1 **
* VIVIANRIBEIRO_Part1_Dataset2.csv	** Dataset 2 **
* VIVIANRIBEIRO_Part1_Dataset3.csv	** Dataset 3 **
* BRAZIL_SOY_2.3.csv ** table downloaded by the part 3 of the download routine **


### Prerequisites
the modules required by this project is:
```
python -m pip install io
python -m pip install os
python -m pip install csv #included in the Python installation
python -m pip install lxml
python -m pip install pandas
python -m pip install zipfile
python -m pip install html5lib
python -m pip install request
python -m pip install urllib.parse
python -m pip install bs4
python -m pip install selenium
python -m pip install unidecode
```
## Version

* First version from February 16, 2019

## Authors

* **Vivian Ribeiro** - *Initial work* - (https://gitlab.com/vivian_rbr)


