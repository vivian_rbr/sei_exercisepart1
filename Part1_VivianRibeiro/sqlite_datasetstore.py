#### SEI Exercise: Part 1 Programming skills
#### Coding date: february 16,2019
#### Author: Vivian Ribeiro
#### Contact author: vivian.ribeiro.bio@gmail.com
#### Details: creating a sqlite data base to place the datasets using pandas module


## From data to sql
import sqlite3
import pandas as pd

cnc = sqlite3.connect('SEIExercise_Part1.sqlite')
df1 = pd.read_csv('VIVIANRIBEIRO_Part1_Dataset1.csv')
df2 = pd.read_csv('VIVIANRIBEIRO_Part1_Dataset2.csv')
df3 = pd.read_csv('VIVIANRIBEIRO_Part1_Dataset3.csv')
df1.to_sql('Dataset1', cnc)
df2.to_sql('Dataset2', cnc)
df3.to_sql('Dataset3', cnc)
cnc.close()