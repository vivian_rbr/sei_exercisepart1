#### SEI Exercise: Part 1 Programming skills
#### Coding date: february 16,2019
#### Author: Vivian Ribeiro
#### Contact author: vivian.ribeiro.bio@gmail.com
#### Disclaimer: git

#packages
import io
import os
import csv
import lxml
import pandas
import zipfile
import html5lib
import requests
import urllib.parse
import pandas as pd
from bs4 import BeautifulSoup
from selenium import webdriver
from unidecode import unidecode
from pandas.compat import StringIO
#options
pd.options.display.max_columns = 50

#------------------------------------------------------------------------------------#
####-- First dataset: A dataset containing informations about Argentina soybean --####
#------------------------------------------------------------------------------------#

### Comand: create a dataset called dataset 1 with informations about soy production
###         and trade desination in Argentina
### First source: informations regarding soybean production for 2016 from agroindustria
### Second source: informations regarding soybean trade destination for 2016 from OPEX

## Collecting data from IDE Agroindustria-Argentina

#Defining parameters for url
urlparams_aar = {'service': 'WFS', 
                 'version': '1.0.0',
                 'request':'GetFeature',
                 'typeName':'minagri:cultivos_soja_produccion_1516',
                 'outputFormat':'csv'}  

#request from url
r_aar = requests.get('http://ide.agroindustria.gob.ar/geoserver/minagri/ows', params=urlparams_aar)
#from text to table using pandas
df_aar = pd.read_csv(StringIO(r_aar.text), sep=',')

## Collecting data from OPEX

#url with parameters
urlparams_aor = ("https://opex.indec.gov.ar/index.php?"+
                 "pagina=mapa_dinamico"+
                 "&anios[]=2016"+
                 "&productos[]=107A"+
                 "&listar_por=origen")

#read_html directly from parameters because the web page is exhibiting the table - should be encoded
df_aor = pd.read_html(urlparams_aor,encoding="ISO8859_1")[2]


##Data combining

#Solving compatibility problem caused by case sensitive
df_aor['Descripción'] = df_aor['Descripción'].str.upper().apply(unidecode)

#Merging data based on Provincias
dtst1 = pd.merge(df_aar, df_aor, how='outer',left_on='provincia',right_on='Descripción')

#Rename columns
dtst1.columns=["FID","PROVINCE","DEPARTMENTS_X","YEAR","SOY(tn)","GEOM","DEPARTMENTS_Y","DOLLARS","NET_THOUSAND(Kg)","RELATIVEDOLARS(%)","RELATIVE_NET_THOUSANDS(%)"]
#Export data as csv
dtst1.to_csv(os.getcwd()+'\\VIVIANRIBEIRO_Part1_Dataset1.csv',sep=",",index=False)

#--------------------------------------------------------------------------------------#
####-- Second dataset - A dataset containing informations about Brazilian soybean --####
#--------------------------------------------------------------------------------------#

### Comand: create a dataset called dataset 2 with informations about soy production
###         and trade desination in Brazil
### First source: informations regarding soybean production from IBGE for 2017
### Second source: informations regarding soybean trade from TRASE for 2017

## First source: informations regarding soybean production for 2017 
urlparams_sidbr = ("https://sidra.ibge.gov.br/geratabela?"+
                   "format=br.csv"+ #csv format
                   "&name=tabela1612.csv"+ #commodities
                   "&terr=N"+
                   "&rank=-"+
                   "&query=t/1612/n6/all/v/214/p/last%201/c81/2713/l/v,p%2Bc81,t")

r_sidbr = requests.get(urlparams_sidbr).content
df_sidbr = pd.read_csv(io.StringIO(r_sidbr.decode('utf-8')), sep=";", skiprows=4, skipfooter=24, header=0)


## Second source: informations regarding soybean trade destination for 2017 

#collecting data from Trase - there is no necessity to fill the form
urlparams_trasebr = ("https://trase.earth/api/v3/contexts/1/download.csv?"+ 
                   "years[]=2017"+ #year
                   "&filters[][name]=SOY_DEFORESTATION_1_YEAR"+ #risk
                   "&filters[][name]=Volume"+ #Volume
                   "&separator=comma"+ #sep
                   "&pivot=1")

r_trasebr = requests.get(urlparams_trasebr)
z_trasebr = zipfile.ZipFile(io.BytesIO(r_trasebr.content))
z_trasebr.extractall()
df_trasebr=pd.read_csv("BRAZIL_SOY_2.3.csv",sep=",",header=0)

#data combining
#Remove symbols from SIDRA to merge the data
#Split SIDRA data in two columns and codify the state label that should be upper case too
#Merge the data based on municipality and state because we know that in Brazil we have municipalities with same name

df_sidbr['Município'] = df_sidbr['Município'].apply(unidecode) #removing symbols
df_sidbr['MunicipioUni'], df_sidbr['EstadoUnilab'] = df_sidbr['Município'].str.split(" \(", 1).str #split string to classifier

#from label to state name using classifier
def classifier(row):
                if row['EstadoUnilab']=='AC)': return 'ACRE'
                elif row['EstadoUnilab']=='AM)': return 'AMAZONAS' 
                elif row['EstadoUnilab']=='AP)': return 'AMAPA' 
                elif row['EstadoUnilab']=='AL)': return 'ALAGOAS' 
                elif row['EstadoUnilab']=='BA)': return 'BAHIA' 
                elif row['EstadoUnilab']=='CE)': return 'CEARA' 
                elif row['EstadoUnilab']=='DF)': return 'DISTRITO FEDERAL' 
                elif row['EstadoUnilab']=='ES)': return 'ESPIRITO SANTO' 
                elif row['EstadoUnilab']=='GO)': return 'GOIAS' 
                elif row['EstadoUnilab']=='MT)': return 'MATO GROSSO' 
                elif row['EstadoUnilab']=='MA)': return 'MARANHAO' 
                elif row['EstadoUnilab']=='MS)': return 'MATO GROSSO DO SUL' 
                elif row['EstadoUnilab']=='MG)': return 'MINAS GERAIS' 
                elif row['EstadoUnilab']=='PB)': return 'PERNAMBUCO' 
                elif row['EstadoUnilab']=='PA)': return 'PARA' 
                elif row['EstadoUnilab']=='PR)': return 'PARANA' 
                elif row['EstadoUnilab']=='PE)': return 'PERNAMBUCO' 
                elif row['EstadoUnilab']=='PI)': return 'PIAUI' 
                elif row['EstadoUnilab']=='RJ)': return 'RIO DE JANEIRO' 
                elif row['EstadoUnilab']=='RN)': return 'RIO GRANDE DO NORTE' 
                elif row['EstadoUnilab']=='RS)': return 'RIO GRANDE DO SUL' 
                elif row['EstadoUnilab']=='RO)': return 'RONDONIA' 
                elif row['EstadoUnilab']=='RR)': return 'RORAIMA' 
                elif row['EstadoUnilab']=='SC)': return 'SANTA CATARINA' 
                elif row['EstadoUnilab']=='SP)': return 'SAO PAULO' 
                elif row['EstadoUnilab']=='SE)': return 'SERGIPE' 
                else: return 'TOCANTINS' 

df_sidbr['EstadoUnilab'] = df_sidbr.apply(classifier, axis=1)

dtst2 = pd.merge(df_sidbr, df_trasebr, how='outer',left_on=['MunicipioUni','EstadoUnilab'],right_on=['MUNICIPALITY','STATE']) #merge dataframes

#rename column names
dtst2.columns=["MUNICIPALITY(STATE)","SOY(tn)","MUNICIPALITY_X","STATE","YEAR","BIOME","STATE","MUNICIPALITY","LOGISTICSHUB","PORT","EXPORTER","IMPORTER","COUNTRY","TYPE","SOY_DEFORESTATION_RISK(Ha)","SOY_EQUIVALENT_TONNES"]
#export data as csv
dtst2.to_csv(os.getcwd()+'\\VIVIANRIBEIRO_Part1_Dataset2.csv',sep=",",index=False)

#--------------------------------------------------------------------------------------#
####---------------- Third dataset - Argentina and Brazil soy trade ----------------####
#--------------------------------------------------------------------------------------#

####- Third dataset - Argentina and Brazil soy trade -####
## First data: National soy production from Argentina and Brazil (Datasets 1 and 2)
## Second source: Trade information from COMTRADE 

#create a dataset with columns: Product,Country,Year,Production from dataset1 and dataset2
dtst1=pd.read_csv("VIVIANRIBEIRO_Part1_Dataset1.csv",sep=",",header=0) #data for Argentina
dtst2=pd.read_csv("VIVIANRIBEIRO_Part1_Dataset2.csv",sep=",",header=0) #data for Brazil

#dataset3 part 1
dtst1nd=dtst1.drop_duplicates(["DEPARTMENTS_X"]) #removing duplicates - maybe this is not necessary
dtst2nd=dtst2.drop_duplicates(["MUNICIPALITY(STATE)","SOY(tn)"]) #removes duplicates
prod_ar=pd.to_numeric(dtst1nd['SOY(tn)'], errors='coerce').sum() #returns the total production after the removal of duplicates
prod_br=pd.to_numeric(dtst2nd['SOY(tn)'], errors='coerce').sum() #returns the total production after the removal of duplicates

#dataset3 part 2 comtrade data - informations regarding trade quantity from comtrade 
driver = webdriver.Chrome()
#Argentina
urlparams_comtrdar=("https://comtrade.un.org/db/dqBasicQueryResults.aspx?"+
                  "cc=120100"+ #Commodity codes
                  "&px=H1"+ #HS
                  "&r=32"+ #country
                  "&y=2017"+ #year
                  "&p=0"+ #world
                  "&rg=2"+ #export
                  "&so=9999")
driver.get(urlparams_comtrdar)
driver.find_element_by_css_selector('#cbRead').click() #check box for license agreement
driver.find_element_by_css_selector('#btContinue').click() #click in continue
user_date = driver.find_elements_by_xpath('//*[@id="dgTradeData"]/tbody/tr[3]/td[9]/div')[0] #element d
date_trdar = user_date.get_attribute('innerText')
num_trdar=pd.to_numeric(date_trdar.replace(',', ''))/1000 #trade quantity exported from Argentina to World in tns
#Brazil
urlparams_comtrdbr=("https://comtrade.un.org/db/dqBasicQueryResults.aspx?"+
                  "cc=120100"+ #Commodity codes
                  "&px=H1"+ #HS
                  "&r=76"+ #country
                  "&y=2017"+ #year
                  "&p=0"+ #world
                  "&rg=2"+ #export
                  "&so=9999")
driver.get(urlparams_comtrdbr)
user_date = driver.find_elements_by_xpath('//*[@id="dgTradeData"]/tbody/tr[3]/td[9]/div')[0] #element d
date_trdbr = user_date.get_attribute('innerText')
num_trdbr=pd.to_numeric(date_trdbr.replace(',', ''))/1000 #trade quantity exported from Brazil to World in tns

driver.close()

d = {'PRODUCT': ['soy','soy'], 'COUNTRY': ['BRAZIL','ARGENTINA'], 'YEAR':['2017','2016'], 'PRODUCTION':[prod_br,prod_ar],'TRADE_QUANTITY':[num_trdbr,num_trdar]} #df structure
df_comtrdarbr = pd.DataFrame(data=d) #formating as data frame
#export data as csv
df_comtrdarbr.to_csv(os.getcwd()+'\\VIVIANRIBEIRO_Part1_Dataset3.csv',sep=",",index=False)
